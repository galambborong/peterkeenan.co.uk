import fs from 'fs';
import matter from 'gray-matter';
import { marked } from 'marked';
import path from 'path';

export interface Frontmatter {
  title: string;
  blurb: string;
  createdAt: string;
  slug: string;
  subTitle?: string;
  published?: boolean;
  lastModified?: string;
  leadImage?: string;
  tags?: string[];
}

export interface PostProps {
  content: string;
  frontmatter: Frontmatter;
}

export const getPosts = () => {
  return fs.readdirSync('posts');
};

export const getFrontMatters = () => {
  const files = getPosts();
  return files.map((file) => {
    const { data } = matter(fs.readFileSync(path.join('posts', file), 'utf8'));
    return data as Frontmatter;
  });
};

export const getSlugs = () => {
  const frontmatters = getFrontMatters();
  return frontmatters.map(({ slug }) => {
    return {
      params: {
        slug
      }
    };
  });
};

export const getFileNameBySlug = (slug: string) => {
  const files = getPosts();
  const [matchingFile] = files.filter((file) => {
    const { data } = matter(fs.readFileSync(path.join('posts', file), 'utf8'));
    if (slug === data.slug) {
      return file;
    }
  });
  return matchingFile;
};

export const getMarkedUpContentAndFrontMatterByFileName = (
  filename: string
): PostProps => {
  const { data, content } = matter(
    fs.readFileSync(path.join('posts', filename), 'utf8')
  );

  return {
    content: marked(content),
    frontmatter: data as Frontmatter
  };
};
