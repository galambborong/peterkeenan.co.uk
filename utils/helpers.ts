import { Frontmatter } from './dataFetchers';

export const keyFormatter = (name: string, index: number) => {
  if (name === undefined || index === undefined) {
    return;
  }
  return `${index}${name.slice(0, 3).toLowerCase().replaceAll(' ', '_')}`;
};

export const convertDateTimeStringToDate = (dateTime: string) => {
  const dateObject = new Date(dateTime);
  return dateObject;
};

export interface MonthDateYear {
  month: string;
  date: string;
  year: string;
}

export const handleDate = (dateString: string) => {
  const data = dateString.split(' ').slice(1);
  const [month, date, year] = data;
  const dateData: MonthDateYear = {
    month,
    date,
    year
  };
  return dateData;
};

export const getDateDetails = (dateObject: Date) => {
  const dateString = dateObject.toDateString();

  return handleDate(dateString);
};

export interface FrontmatterYearGroup {
  year: number;
  frontmatters: Frontmatter[];
}

export const groupPostsByYear = (
  year: number,
  frontmatters: Frontmatter[]
): FrontmatterYearGroup => {
  const frontMattersByYear = frontmatters.filter((data) => {
    const { createdAt } = data;
    const dateTimeObject = convertDateTimeStringToDate(createdAt);
    if (dateTimeObject.getFullYear() === year) {
      return data;
    }
  });
  return {
    year,
    frontmatters: frontMattersByYear
  };
};

export const groupAllPostsByYear = (frontmatters: Frontmatter[]) => {
  const currentYear = new Date().getFullYear();
  const groupedPosts = [];
  for (let i = 2016; i <= currentYear; i++) {
    const posts = groupPostsByYear(i, frontmatters);
    if (posts.frontmatters.length > 0) {
      groupedPosts.push(posts);
    }
  }
  return groupedPosts;
};

export function postsGroupedByYears(posts: Frontmatter[]) {
  const postsGrouped = groupAllPostsByYear(posts).sort((a, b) => {
    return b.year - a.year;
  });

  const groupedPosts = postsGrouped.map((yearContent: FrontmatterYearGroup) => {
    yearContent.frontmatters.sort((yearA, yearB) => {
      const dateA = convertDateTimeStringToDate(yearA.createdAt);
      const dateB = convertDateTimeStringToDate(yearB.createdAt);
      return dateB.getTime() - dateA.getTime();
    });
    return yearContent;
  });
  return groupedPosts;
}

export function postsGroupedByYearsInvert(posts: Frontmatter[]) {
  const postsGrouped = groupAllPostsByYear(posts).sort((a, b) => {
    return a.year - b.year;
  });

  const groupedPosts = postsGrouped.map((yearContent: FrontmatterYearGroup) => {
    yearContent.frontmatters.sort((yearA, yearB) => {
      const dateA = convertDateTimeStringToDate(yearA.createdAt);
      const dateB = convertDateTimeStringToDate(yearB.createdAt);
      return dateA.getTime() - dateB.getTime();
    });
    return yearContent;
  });
  return groupedPosts;
}
