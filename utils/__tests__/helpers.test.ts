import {
  getDateDetails,
  convertDateTimeStringToDate,
  keyFormatter,
  MonthDateYear
} from '../helpers';

describe('keyFormatter', () => {
  test('Return undefined when passed no args', () => {
    // @ts-ignore
    expect(keyFormatter()).toBeUndefined();
  });

  test('Return string, second parameter combined with first, sliced and lowercased', () => {
    let expectedOut = '0mus';
    let actualOut = keyFormatter('Music', 0);
    expect(actualOut).toBe(expectedOut);

    expectedOut = '11res';
    actualOut = keyFormatter('Research Activities', 11);
    expect(actualOut).toBe(expectedOut);
  });

  test('Replace space characters with _', () => {
    const expectedOut = '9a_f';
    const actualOut = keyFormatter('A FAntastic post', 9);
    expect(actualOut).toBe(expectedOut);
  });
});

describe('handleDateTimeString', () => {
  test('Return undefined when passed no args', () => {
    // @ts-ignore
    expect(convertDateTimeStringToDate()).toBeUndefined();
  });

  test('Convert string to Date object', () => {
    const input = '2021-12-25T10:10:10.010Z';
    const actualOut = convertDateTimeStringToDate(input);
    console.log(actualOut);
    expect(actualOut).toBeInstanceOf(Date);
  });
});

describe('extractDateDetails', () => {
  test('Return array of strings', () => {
    const input = convertDateTimeStringToDate('2021-12-25T10:10:10.010Z');
    const expectedOutput: MonthDateYear = {
      month: 'Dec',
      date: '25',
      year: '2021'
    };
    const actualOutput = getDateDetails(input);
    expect(actualOutput).toEqual(expectedOutput);
  });
});
