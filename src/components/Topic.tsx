import Link from 'next/link';
import styles from '../../styles/Topic.module.scss';
import { keyFormatter } from '../../utils/helpers';

interface TopicItem {
  mainText: string;
  url: string;
  icon?: string;
}

export interface TopicProps {
  topic: string;
  items: TopicItem[];
}

export default function Topic({ topic, items }: TopicProps) {
  return (
    <section className={styles.topicContainer}>
      <h3 className={styles.topic}>{topic}</h3>
      <ul>
        {items.map((item, i) => {
          return (
            <li
              key={keyFormatter(item.mainText, i)}
              className={styles.topicItem}
            >
              <Link href={item.url}>{item.mainText}</Link>
            </li>
          );
        })}
      </ul>
    </section>
  );
}
