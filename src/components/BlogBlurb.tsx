import styles from '../../styles/BlogBlurb.module.scss';

interface BlogBlurbProps {
  blurb: string;
}

export default function BlogBlurb({ blurb }: BlogBlurbProps) {
  return (
    <div className={styles.blurb} dangerouslySetInnerHTML={{ __html: blurb }} />
  );
}
