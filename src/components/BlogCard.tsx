import Link from 'next/link';
import React from 'react';
import { Frontmatter } from '../../utils/dataFetchers';
import {
  getDateDetails,
  convertDateTimeStringToDate,
  keyFormatter
} from '../../utils/helpers';
import styles from '../../styles/BlogCard.module.scss';

interface BlogCardProps {
  frontmatter: Frontmatter;
}

export default function BlogCard({ frontmatter }: BlogCardProps) {
  const { title, createdAt, slug } = frontmatter;

  const { month, date } = getDateDetails(
    convertDateTimeStringToDate(createdAt)
  );

  return (
    <article className={styles.card}>
      <p className={styles.date}>{`${month} ${date}`}</p>
      <Link href={`/blog/${slug}`}>
        <a className={styles.title}>{title}</a>
      </Link>
      {frontmatter.tags && (
        <ul className={styles.tags}>
          {frontmatter.tags.map((tag, i) => {
            return (
              <li className={styles.tag} key={keyFormatter(tag, i)}>
                #{tag}
              </li>
            );
          })}
        </ul>
      )}
    </article>
  );
}
