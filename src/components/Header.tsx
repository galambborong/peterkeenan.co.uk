import styles from '../../styles/Header.module.scss';
import { useRouter } from 'next/router';

interface HeaderProps {
  headerContent: HeaderContent;
}

interface HeaderContent {
  mainHeader?: string;
  subHeader?: string;
  imageLink?: ImageInfo;
}

interface ImageInfo {
  id: string;
  path: string;
  alt: string;
}

export default function Header({ headerContent }: HeaderProps) {
  const { mainHeader, subHeader } = headerContent;
  const { route } = useRouter();

  const isRouteRoot = route === '/';
  const titleClassChoice = isRouteRoot
    ? styles.headerHeroTitle
    : styles.headerTitle;
  const subClassChoice = isRouteRoot ? styles.headerHeroSub : styles.headerSub;

  return (
    <header className={styles.header}>
      {headerContent.mainHeader && (
        <h1 className={titleClassChoice} data-testid="main">
          {mainHeader}
        </h1>
      )}
      {headerContent.imageLink && (
        <img
          data-testid="image"
          id={headerContent.imageLink?.id}
          src={headerContent.imageLink?.path}
          alt={headerContent.imageLink?.alt}
        />
      )}
      {subHeader && (
        <h2 className={subClassChoice} data-testid="sub">
          {subHeader}
        </h2>
      )}
    </header>
  );
}
