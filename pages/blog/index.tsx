import { GetStaticProps } from 'next';
import Header from '../../src/components/Header';
import {
  keyFormatter,
  FrontmatterYearGroup,
  postsGroupedByYears
} from '../../utils/helpers';
import { Frontmatter, getFrontMatters } from '../../utils/dataFetchers';
import styles from '../../styles/BlogIndex.module.scss';
import React, { useState } from 'react';
import BlogYear from '../../src/components/BlogYear';
import BlogBlurb from '../../src/components/BlogBlurb';

interface BlogIndexProps {
  frontmatters: Frontmatter[];
}

export default function BlogIndex({ frontmatters }: BlogIndexProps) {
  const headerContent = {
    mainHeader: 'My wee blog',
    subHeader: 'Code, music, bikes and coffee'
  };

  const [currentFrontMattersOrder, setCurrentFrontMattersOrder] = useState<
    FrontmatterYearGroup[]
  >(() => postsGroupedByYears(frontmatters));

  const blurb =
    "This is not the first blog I've started in some shape or form, so whether this project lives or dies remains to be seen. What I can promise, however, is that it's very unlikely that anything here will be of much interest to you &hellip; but <strong>welcome, nonetheless!</strong>";

  return (
    <>
      <Header headerContent={headerContent} />
      <section>
        <BlogBlurb blurb={blurb} />
        <p>
          The main motivation for this space is to encourage me to apply some
          rigour to my own learning. Taking a concept and trying to summarise it
          in my own way is how I learn.
        </p>
        <p>
          It's also quite possible that the odd off-topic piece will appear
          here. I love coffee, and am constantly trying to improve my coffee
          brewing skills. So, odds are that will feature!
        </p>
      </section>
      <section className={styles.blogCards}>
        {currentFrontMattersOrder.map((yearContent, i) => {
          return (
            <BlogYear
              key={keyFormatter(yearContent.year.toString(), i)}
              detail={yearContent}
            />
          );
        })}
      </section>
    </>
  );
}

export const getStaticProps: GetStaticProps = async () => {
  return {
    props: {
      frontmatters: getFrontMatters()
    }
  };
};
