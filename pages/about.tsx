import Link from 'next/link';
import Header from '../src/components/Header';
import styles from '../styles/About.module.scss';

export default function About() {
  const headerContent = {
    mainHeader: 'A little about me',
    subHeader: 'musician // developer'
  };

  return (
    <>
      <Header headerContent={headerContent} />
      <section id="dev">
        <h3 className={styles.sectionTitle}>Dev</h3>
        <article id="quickfire">
          <p>
            I'm relatively new to dev: I worked in the music world for many
            years (see below). I decided to change direction and signed on to a
            coding bootcamp with Northcoders in January 2021.
          </p>
        </article>
        <article id="work">
          <p>
            I'm an Associate Developer at{' '}
            <Link href={'https://sage.com'}>
              <a className="external">Sage</a>
            </Link>
            , working on their Digital Network project as part of the Trust
            Fabric team. We primarily work in C#, with a smattering of
            Python/JavaScript here and there, and a lot of AWS, very much
            serverless. It's a great place to be &ndash; interesting work and
            technologies (and great people!).
          </p>
        </article>
        <article id="fun">
          <p>
            As a developer, there are a few things which make me tick. I think
            I'm a front end developer at heart as I love playing with React
            (NextJS to be specific) and have been learning Svelte recently and
            really enjoy it.
          </p>
          <p>
            I'm pretty into functional programming, so play in F# over on{' '}
            <Link href={'https://exercism.org/profiles/galambborong'}>
              <a className="external">Exercism</a>
            </Link>
            . Other languages I'm interested in include Rust and Haskell.
          </p>
        </article>
      </section>
      <section id="music">
        <h3 className={styles.sectionTitle}>Music</h3>
        <article id="odl">
          <p>
            I've taken on numerous types of work in the music world. I did some
            specialist music engraving for music publishers for a few years. I
            also worked in arts administration for a time.
          </p>
          <h4 className={styles.subSectionTitle}>
            Opera dei Lumi and Research
          </h4>
          <p>
            My main drive and energy as a musician focused around researching
            and performing music from the mid- to late-eighteenth century.{' '}
          </p>
          <p>
            I founded and am Artistic Director of{' '}
            <Link href="https://operadeilumi.org.uk">
              <a className="external">Opera dei Lumi</a>
            </Link>
            , a group specialising in repertoire from the Enlightenment period.
          </p>
          <p>
            I did a MMus research degree on Mozart's tempo markings, and have
            had bits and bobs of writings published here and there, which you
            can read <Link href="#">here</Link>
          </p>
        </article>
      </section>
    </>
  );
}
